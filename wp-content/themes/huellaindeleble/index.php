<?php get_header(); ?>
<!-- Home carousel -->
<div class="container">
	<div class="home-carousel">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <!--<ol class="carousel-indicators">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
		  </ol>-->

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
		    <div class="item active">
		      <a href="javascript:void(0);">
		      	<img src="<?php echo get_template_directory_uri();?>/images/proof-images/banner1.jpg" alt="">
		      </a>
		    </div>
		    <div class="item">
		      <a href="javascript:void(0);">
		      	<img src="<?php echo get_template_directory_uri();?>/images/proof-images/banner2.jpg" alt="">
		      </a>
		    </div>
		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</div>
</div>
<!-- Notices Box -->
<div class="home-notices-box">
	<div class="container">
		<div class="row">
			<?php
				for($i=1; $i<=4; $i++)
				{
			?>
				<div class="notice col-md-6 col-sm-6 col-xs-12">
					<div class="row">
						<div class="notice-image col-md-6 col-sm-6 col-xs-12">
							<a href="javascript:void(0);">
								<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/proof-images/notice-image.jpg">
							</a>
						</div>
						<div class="notice-text col-md-6 col-sm-6 col-xs-12">
							<a href="javascript:void(0);">
								<span class="title"> Título Noticia<?php echo $i;?></span>
								<span class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
							</a>
						</div>
					</div>
				</div>
			<?php 
				}
			?>
			
		</div>
	</div>
</div>


<?php get_header(); ?>